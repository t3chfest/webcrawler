import urllib2
import ssl
from BeautifulSoup import BeautifulSoup
from pymongo import MongoClient
from datetime import datetime


#autingo
pagerautingo = urllib2.urlopen('http://recambios-coche.autingo.es/juego-de-pastillas-de-freno-con-accesorios-con-tornillos-lucas-girling-bosch-0986461769').read()
soup = BeautifulSoup(pagerautingo)
soup.prettify()

autingoprice = soup.findAll('span', {'class' : 'price', 'itemprop': 'price'})[0].text
print autingoprice

#Oscaro
hdr = {'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.64 Safari/537.11',
    'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
    'Accept-Charset': 'ISO-8859-1,utf-8;q=0.7,*;q=0.3',
    'Accept-Encoding': 'none',
    'Accept-Language': 'en-US,en;q=0.8',
    'Connection': 'keep-alive'}

req = urllib2.Request('https://www.oscaro.es/juego-de-4-pastillas-de-freno-bosch-0-986-461-769-83335-402-p', headers=hdr)
pageoscaro = urllib2.urlopen(req).read()
souposcaro = BeautifulSoup(pageoscaro)
souposcaro.prettify()
oscaroprice = souposcaro.findAll('div', {'class' : 'productfile__pricebox__price--TTC' })[0].text

oscaroprice = oscaroprice.split(" ")[0]

print oscaroprice

#amazon                 <span id="priceblock_ourprice" class="a-size-medium a-color-price">EUR 26,69</span>
reqamazon = urllib2.Request('https://www.amazon.es/Bosch-986461769-juego-pastillas-frenos/dp/B006F1QS2W/ref=sr_1_1?ie=UTF8&qid=1506783054&sr=8-1&keywords=0986461769', headers=hdr)
pageamazon = urllib2.urlopen(reqamazon).read()
soup = BeautifulSoup(pageamazon)
soup.prettify()

amazonprice = soup.findAll('span', {'id' : 'priceblock_ourprice'})[0].text
amazonprice = amazonprice.split(" ")[1]
print amazonprice